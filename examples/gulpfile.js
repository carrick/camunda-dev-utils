require('dotenv-extended').load();
var gulp = require('gulp');
const cutils = require('camunda-dev-utils');

gulp.task('clean-instances', () => {
  return cutils.killInstances();
});

gulp.task('default', function() {
  gulp.watch('bpmn/*.bpmn', ['clean-instances']).on('change', function(event) {
    console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    cutils.deploy(event.path);
  });
});



