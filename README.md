# camunda-dev-utils

Utiliities for camumda bpmn development in node.

# Install
    npm i -save-dev git+https://gitlab.com/carrick/camunda-dev-utils.git

# Prerequesites

You must set the 'CAMUNDA\_API\_BASE\_URL'
environment variable:

    CAMUNDA_API_BASE_URL=http://example.com/engine-rest

# Usage

See examples/gulpfile.js.

