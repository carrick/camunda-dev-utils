const req = require('request-promise-native');
const CamSDK = require('camunda-bpm-sdk-js');
var fs = require('fs');
var path = require('path');
var logger = require('logger');

const request = req.defaults({
  baseUrl:   process.env.CAMUNDA_API_BASE_URL,
  json: true,
});

var camClient = new CamSDK.Client({
  mock: false,
  // the following URL does not need authentication,
  // but the tradeof is that some requests will fail
  // e.g.: some filters use the reference to the user performing the request
  apiUri: process.env.CAMUNDA_API_BASE_URL
});

var deploymentService = new camClient.resource('deployment');


// returns an array of functions reading the content of files.
// To be used in CamSDK.utils.series()
function readFiles(filenames) {
  return filenames.map(function (filename) {
    return function (cb) {
      fs.readFile(filename, function (err, content) {
        if (err) { return cb(err); }

        cb(null, {
          name: filename,
          content: content.toString()
        });
      });
    };
  });
}
function deploymentName() {
  return `deploy${new Date().getTime().toString()}`;
}

function thr(err) {
  if (err) {
    throw err;
  }
}

function toArray(obj) {
  var arr = [];
  Object.keys(obj).forEach(function (key) {
    arr.push(obj[key]);
  });
  return arr;
}
module.exports = {
  killInstances: () => {
    return request("/process-instance")
    .then(result => {
      logger.info("Found", result.length, "running instances.");
      return Promise.all( result.map( instance => request.del('/process-instance/' + instance.id )));
    })
    .then(result => {
      logger.info("Cleanup complete.");
    })
    .catch(error => {
      logger.error(error);
    });
},

deploy: (path) => { //TODO: Unhardcode this
  CamSDK.utils.series(readFiles([path]), function (err, files) {
      thr(err);

      console.log("files:", files);

      // create a deployment with..

      //fs.writeFileSync("/tmp/foo.txt", JSON.toString(options), JSON.toString(files));

      // var r = repl.start("node> ");

      deploymentService.create({

        // ... the settings
        deploymentName: deploymentName(),
        enableDuplicateFiltering: "yes",
        deployChangedOnly: "yes",
        // ... and the files
        files: toArray(files)
      }, function (err, deployment) {
        thr(err);

        console.log('deployment "' + deployment.name + '" succeeded, ' + deployment.deploymentTime);
      });
    });
}
};
